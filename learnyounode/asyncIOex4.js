// program counts number of new lines in file using async filesystem

var fs = require('fs');  //import fs module

// call read file on commandline argument, utf8 used to return string rather than
// buffer, then callback function called
fs.readFile(process.argv[2], 'utf8', function(err, fileContents){
	if (err){
		console.log(err);      
	}
    console.log(fileContents.split('\n').length -1);
});
// Program returns number of lines in argument file

var fs = require('fs')    // import fs module

var fileBuffer = fs.readFileSync(process.argv[2]);    // store buffer of file
var lineCount = fileBuffer.toString().split('\n').length -1 // -1 used as file not terminated with new line charcater
console.log(lineCount)
